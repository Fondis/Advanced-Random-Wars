﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof (MeshFilter))]
[RequireComponent(typeof (MeshRenderer))]
[RequireComponent(typeof (MeshCollider))]
public class MapGenerator : MonoBehaviour
{
    public int SizeY;
    public int SizeX;
    public int Margin;
    public int TileResolution;
    public Texture2D TerrainTiles;
    //private Texture2D texture;


    public float TileSize;
    public MapData MapData;
    public Texture2D Texture;
    public static MapGenerator Instance;

    void Awake()
    {
        SizeX = MapData.SizeX;
        SizeY = MapData.SizeY;
        TerrainTiles = MapData.SpriteSheet;
        TileResolution = MapData.TileResolution;
        Instance = this;
    }

    // Use this for initialization
    private void Start()
    {
        BuildMesh();
    }

    private Color[][] ChopUpTiles()
    {
        var numTilesPerRow = (TerrainTiles.width+Margin)/(TileResolution + Margin);
        var numRows = (TerrainTiles.height+Margin)/(TileResolution + Margin);

        var tiles = new Color[numTilesPerRow*numRows][];

        for (int y = 0; y < numRows; y++)
        {
            for (var x = 0; x < numTilesPerRow; x++)
            {
                tiles[(numRows - 1 - y) *numTilesPerRow + x] = TerrainTiles.GetPixels(x*TileResolution + Margin*x, y*TileResolution + Margin*y, TileResolution, TileResolution);
            }
        }
        return tiles;
    }

    /// <summary>
    /// Create the map's texture
    /// </summary>
    public Texture2D BuildTexture()
    {
        var texWidth = SizeX*TileResolution;
        var texHeight = SizeY*TileResolution;
        var texture = new Texture2D(texWidth, texHeight);

        var tiles = ChopUpTiles();

        for (int y = 0; y < SizeY; y++)
        {
            for (int x = 0; x < SizeX; x++)
            {
               var p = tiles[MapData.GetTileAt(x, y)];
               texture.SetPixels(x*TileResolution, y*TileResolution, TileResolution, TileResolution, p);
            }
        }

        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        Texture = texture;
        return texture;
    }

    public void ApplyTexture()
    {
        var tex = BuildTexture();
        tex.Apply();
       // var meshRenderer = GetComponent<MeshRenderer>();
       // meshRenderer.sharedMaterials[0].mainTexture = tex;
    }

    /// <summary>
    /// Build the mesh of the map plane, triangle by triangle
    /// </summary>
    public void BuildMesh()
    {
        int numTiles = SizeX*SizeY;
        int numTris = numTiles*2;

        int vSizeX = SizeX + 1;
        int vSizeY = SizeY + 1;
        int numVerts = vSizeX*vSizeY;

        Vector3[] vertices = new Vector3[numVerts];
        Vector3[] normals = new Vector3[numVerts];
        Vector2[] uv = new Vector2[numVerts];

        int[] triangles = new int[numTris*3];

        int x, y;
        
        //Vertices
        for (y = 0; y < vSizeY; y++)
        {
            for (x = 0; x < vSizeX; x++)
            {
                vertices[y*vSizeX + x] = new Vector3(x*TileSize, -y*TileSize, 0);
                normals[y*vSizeX + x] = Vector3.down;
                uv[y*vSizeX + x] = new Vector2((float) x/SizeX, 1f - (float) y/SizeY); //Need the '1f' part to make sure the tiles are the right way around.
            }
        }

        //Triangles
        for (y = 0; y < SizeY; y++)
        {
            for (x = 0; x < SizeX; x++)
            {
                var squareIndex = y*SizeX + x;
                var triOffset = squareIndex*6;

                triangles[triOffset + 0] = y * vSizeX + x + 0;
                triangles[triOffset + 1] = y * vSizeX + x + vSizeX + 1;
                triangles[triOffset + 2] = y * vSizeX + x + vSizeX + 0;

                triangles[triOffset + 3] = y*vSizeX + x + 0;
                triangles[triOffset + 4] = y*vSizeX + x + 1;
                triangles[triOffset + 5] = y*vSizeX + x + vSizeX + 1;
            }
        }
        
        //Add the vertices and triangles to a mesh
        var mesh = new Mesh
        {
            vertices = vertices,
            triangles = triangles,
            uv = uv,
            //normals = normals
        };

        mesh.RecalculateNormals();

       // var meshFilter = GetComponent<MeshFilter>();
        //var meshCollider = GetComponent<MeshCollider>();

       // meshFilter.mesh = mesh;
        //meshCollider.sharedMesh = mesh;

        //TODO I need to create a second script that I can attach to a game object, to generate the map. Or maybe I don't? Guess we'll find out.

        ApplyTexture();
    }
}