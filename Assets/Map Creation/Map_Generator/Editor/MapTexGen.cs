﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.Assertions;

[ExecuteInEditMode]
public class MapTexGen
{
    public static MapTexGen Instance;

    public int SizeY;
    public int SizeX;
    //private Texture2D texture;

    public float TileSize;
    public Texture2D TerrainTiles;

    public MapData MapData;
    public Texture2D Texture;

    public MapTexGen(MapData map)
    {
        Assert.IsNotNull(map);
        MapData = map;
        TerrainTiles = map.SpriteSheet;
        SizeX = MapData.SizeX;
        SizeY = MapData.SizeY;
        Instance = this;

        BuildTexture();
    }
    private Color[][] ChopUpTiles()
    {
        var numTilesPerRow = (TerrainTiles.width + MapData.Margin) / (MapData.TileResolution + MapData.Margin);
        var numRows = (TerrainTiles.height + MapData.Margin) / (MapData.TileResolution + MapData.Margin);

        var tiles = new Color[numTilesPerRow * numRows][];

        for (int y = 0; y < numRows; y++)
        {
            for (var x = 0; x < numTilesPerRow; x++)
            {
                tiles[(numRows - 1 - y) * numTilesPerRow + x] = TerrainTiles.GetPixels(x * MapData.TileResolution + MapData.Margin * x, y * MapData.TileResolution + MapData.Margin * y, MapData.TileResolution, MapData.TileResolution);
            }
        }
        return tiles;
    }

    /// <summary>
    /// Create the map's texture
    /// </summary>
    public Texture2D BuildTexture()
    {
        var texWidth = SizeX * MapData.TileResolution;
        var texHeight = SizeY * MapData.TileResolution;
        var texture = new Texture2D(texWidth, texHeight);

        var tiles = ChopUpTiles();

        for (int y = 0; y < SizeY; y++)
        {
            for (int x = 0; x < SizeX; x++)
            {
                var p = tiles[MapData.GetTileAt(x, y)];
                texture.SetPixels(x * MapData.TileResolution, y * MapData.TileResolution, MapData.TileResolution, MapData.TileResolution, p);
            }
        }

        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        Texture = texture;
        return texture;
    }
}