﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CreateAssetMenu(menuName = "New Map", order = 1)]
public class MapData : ScriptableObject
{
    int[,] _mapData;
    public int SizeX = 16;
    public int SizeY = 16;

    public int Margin = 1;
    public int TileResolution = 16;
    public Texture2D SpriteSheet;

    //MapGenerator MapGen = MapGenerator.Instance;
    
    void OnEnable()
    {
        _mapData = new int[SizeX, SizeY];
        //texture = new Texture2D(sizeX, sizeY);
        //MapGen.BuildTexture();
    }

    public int GetTileAt(int x, int y)
    {
        return _mapData[x, y];
    }

    public void SetTileAt(int x, int y, int newTile)
    {
        _mapData[x, y] = newTile;
    }

    public MapData(int sizeX, int sizeY)
    {
        _mapData = new int[sizeX, sizeY];

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                _mapData[x, y] = 1;
            }
        }
    }
}
