﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MapGenerator))]
public class TileMapGeneratorButton : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Re-Generate"))
        {
            MapGenerator generator = (MapGenerator) target;
            generator.BuildMesh();
        }
    }

}
