﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class CanvasTextureGen : MonoBehaviour
{
    int SizeY;
    int SizeX;
    public int Margin = 1;
    public float TileSize;
    public Texture2D TerrainTiles;
    public int TileResolution;
    public MapData MapData;

    void Awake()
    {
        SizeX = MapData.SizeX;
        SizeY = MapData.SizeY;
    }

    // Use this for initialization
    private void Start()
    {
        BuildTexture();
    }

    private Color[][] ChopUpTiles()
    {
        var numTilesPerRow = (TerrainTiles.width+Margin)/(TileResolution + Margin);
        var numRows = (TerrainTiles.height+Margin)/(TileResolution + Margin);

        var tiles = new Color[numTilesPerRow*numRows][];

        for (int y = 0; y < numRows; y++)
        {
            for (var x = 0; x < numTilesPerRow; x++)
            {
                tiles[(numRows - 1 - y) *numTilesPerRow + x] = TerrainTiles.GetPixels(x*TileResolution + Margin*x, y*TileResolution + Margin*y, TileResolution, TileResolution);
            }
        }
        return tiles;
    }

    /// <summary>
    /// Create the map's texture
    /// </summary>
    public Texture2D BuildTexture()
    {
        var texWidth = SizeX*TileResolution;
        var texHeight = SizeY*TileResolution;
        var texture = new Texture2D(texWidth, texHeight);

        var tiles = ChopUpTiles();

        for (int y = 0; y < SizeY; y++)
        {
            for (int x = 0; x < SizeX; x++)
            {
               var p = tiles[MapData.GetTileAt(x, y)];
               texture.SetPixels(x*TileResolution, y*TileResolution, TileResolution, TileResolution, p);
            }
        }

        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        return texture;
    }
}