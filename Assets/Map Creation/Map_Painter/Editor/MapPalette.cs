﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MapPalette : EditorWindow
{
    public static MapPalette Instance;

    //The scale options available for the tile palette
    public enum Scale
    {
        x1 = 1,
        x2 = 2,
        x3 = 3,
        x4 = 4,
        x5 = 5,
        x10 = 9
    }

    void Awake()
    {
        Instance = this;
    }

    private Scale scale;
    private Vector2 currentSelection = Vector2.zero;
    public Vector2 scrollPosition = Vector2.zero;
    private MapTexGen selection;
    private Texture2D tex;
    public int paintTile = 0;
  
    void OnGUI()
    {
        //If an object is selected, get the MapGenerator script component.
        //TODO figure out why it errors when the project is first opened.
        selection = MapTexGen.Instance;

        if (selection != null)
        {
            var texture2D = selection.TerrainTiles;
            if (texture2D != null)
            {
                //GUI scale picker. Capture the current scale, and create a vector2d to scale.
                scale = (Scale) EditorGUILayout.EnumPopup("Zoom", scale);
                var newScale = (int) scale;
                var newTextureSize = new Vector2(texture2D.width, texture2D.height)*newScale;

                var x_offset = 10;
                var y_offset = 25;
                var offset = new Vector2(x_offset, y_offset); //The origin of the actual map palette.


                var viewport = new Rect(0, y_offset, position.width, position.height - y_offset); //The box the scrollbars surround.
                var contentSize = new Rect(0, y_offset, newTextureSize.x, newTextureSize.y + offset.y);
                scrollPosition = GUI.BeginScrollView(viewport, scrollPosition, contentSize); //Create the actual scrollbars TODO figure out why this breaks everything?
                GUI.DrawTexture(new Rect(offset.x, offset.y, newTextureSize.x, newTextureSize.y), texture2D); //Draw the texture in the box.

                //Calculate the size of each tile to scale, as well as the margin; create a vector to hold the grid of tiles.
                int tileX = selection.MapData.SizeX*newScale;
                int tileY = selection.MapData.SizeY*newScale;
                int newMargin = selection.MapData.Margin*newScale;
                var grid = new Vector2((newTextureSize.x+newMargin)/(tileX+newMargin), (newTextureSize.y+newMargin)/(tileY+newMargin)); //Builds the grid of tiles to select.

                //Holds the position of the current selection; calculated by multiplying the selected tile with the tilesize, taking offset into account.
                var selectionPos = new Vector2(tileX*currentSelection.x + offset.x, tileY*currentSelection.y + offset.y);

                //Create the selection box - texture; style; then final GUI box.
                var boxTex = new Texture2D(1, 1);
                boxTex.SetPixel(0, 0, new Color(0, 0.5f, 1f, 0.4f));
                boxTex.Apply();
                var style = new GUIStyle(GUI.skin.customStyles[0]) {normal = {background = boxTex}};
                GUI.Box(new Rect(selectionPos.x + selection.MapData.Margin * currentSelection.x * newScale, selectionPos.y + selection.MapData.Margin * currentSelection.y * newScale, tileX, tileY), "", style);


                //On mouse click
                var cEvent = Event.current;
                Vector2 mousePos = new Vector2(cEvent.mousePosition.x, cEvent.mousePosition.y);
                if (cEvent.type == EventType.mouseDown && cEvent.button == 0)
                {
                    //current selection is calculated by dividing the mouse position by the scale of the tiles, taking margins into account.
                    currentSelection.x = Mathf.Floor((mousePos.x + newMargin - offset.x) / (tileX + newMargin));
                    currentSelection.y = Mathf.Floor((mousePos.y + newMargin - offset.y) / (tileY + newMargin));

                    //If the selection is outside the texture, clamp to the edge.
                    if (currentSelection.x > grid.x - 1)
                    {
                        currentSelection.x = Mathf.Floor(grid.x - 1);
                    }
                    if (currentSelection.y > grid.y - 1)
                    {
                        currentSelection.y = Mathf.Floor(grid.y - 1);
                    }

                    if (currentSelection.x < 0)
                    {
                        currentSelection.x = 1;
                    }
                    if (currentSelection.y < 0)
                    {
                        currentSelection.y = 1;
                    }

                    //MapData.SetTileAt(10, 10, (int)(currentSelection.x + currentSelection.y * grid.x));
                    paintTile = (int) (currentSelection.x + currentSelection.y*grid.x);
                    tex = selection.BuildTexture();
                    tex.Apply();
                    Repaint();
                }
                GUI.EndScrollView();
            }
        }
    }
}
