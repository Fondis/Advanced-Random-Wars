﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CreateAssetMenu(menuName = "New Unit", order = 1)]
public class UnitData : ScriptableObject
{
    private Texture2D sprite;
    public int health;
    public int attack;
    public int move;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
